import os
from analysis_tools.utils import import_root
import correctionlib

ROOT = import_root()
correctionlib.register_pyroot_binding()

json_path = "/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/LUM/{}/puWeights.json.gz"

class puWeightRDFProducer():
    def __init__(self, *args, **kwargs):
        self.year = int(kwargs.pop("year"))
        self.isUL = kwargs.pop("isUL")
        self.isMC = kwargs.pop("isMC")

        if self.isMC:
            if not self.isUL:
                raise ValueError("This module can't be used for preUL, use puWeight instead")
            if self.year == 2018:
                filename = json_path.format("2018_UL")
            else:
                raise ValueError("2016 and 2017 not yet implemented")

        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
        ROOT.gInterpreter.ProcessLine(
            'auto corr = MyCorrections("%s", "Collisions%s_UltraLegacy_goldenJSON");'
                % (filename, str(self.year)[-2:]))

    def run(self, df):
        branches = ['puWeight2', 'puWeightUp2', 'puWeightDown2']
        for branch_name, syst in zip(branches, ["nominal", "up", "down"]):
            df = df.Define(branch_name, 'corr.eval({Pileup_nTrueInt, "%s"})' % syst)
        return df, branches


def puWeightRDF(**kwargs):
    return lambda: puWeightRDFProducer(**kwargs)
